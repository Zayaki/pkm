import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticleComponent } from './article/article.component';
import { GenerationsComponent } from './composants/generations/generations.component';
import { PokemonsComponent } from './composants/pokemons/pokemons.component';

 // locale : France 
import { LOCALE_ID } from '@angular/core'; 
import { registerLocaleData } from '@angular/common'; 
import localeFR from '@angular/common/locales/fr'; 
registerLocaleData(localeFR); 

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    GenerationsComponent,
    PokemonsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
